#ifndef KTE_TOOLBAR_H_
#define KTE_TOOLBAR_H_

namespace kte {

extern int toolbar_height;

void make_toolbar(void);
void make_save_dialog(void);
void display_save_dialog(void);

};


#endif // KTE_TOOLBAR_H_
