#include <vector>
#include <string>
#include <fstream>

#include "imgui.h"
#include "imgui_stdlib.h"
#include "imfilebrowser.h"
#include "toolbar.h"
#include "cpp_defer.h"
#include "file_browser.h"
#include "editor.h"

int kte::toolbar_height = 0;
std::string save_dialog_buf;

void write_editor_buf(void)
{
    std::ofstream out(kte::editor_path);
    out << kte::editor_buf;
}

void kte::make_toolbar(void)
{
    ImGui::BeginMainMenuBar();
    defer { ImGui::EndMainMenuBar(); };

    ImVec2 size = ImGui::GetWindowSize();
    kte::toolbar_height = size.y;
    if (ImGui::BeginMenu("File")) {
        if (ImGui::MenuItem("Open")) {
            kte::file_browser.Open();
        }
        if (ImGui::MenuItem("Save")) {
            write_editor_buf();
        }
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Settings")) {
        if (ImGui::BeginMenu("Themes")) {
            if (ImGui::MenuItem("imgui_dark")) {
                ImGui::StyleColorsDark();
            }
            if (ImGui::MenuItem("imgui_light")) {
                ImGui::StyleColorsLight();
            }
            if (ImGui::MenuItem("imgui_classic")) {
                ImGui::StyleColorsClassic();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenu();
    }
}

