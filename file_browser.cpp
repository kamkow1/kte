#include "imgui.h"
#include "imfilebrowser.h"
#include "file_browser.h"

ImGui::FileBrowser kte::file_browser(
    ImGuiFileBrowserFlags_CreateNewDir
    | ImGuiFileBrowserFlags_ConfirmOnEnter
    | ImGuiFileBrowserFlags_EnterNewFilename
    | ImGuiFileBrowserFlags_CloseOnEsc
);

