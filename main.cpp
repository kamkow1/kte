#include <GLFW/glfw3.h>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "imfilebrowser.h"

#include "cpp_defer.h"
#include "toolbar.h"
#include "file_browser.h"
#include "editor.h"

int main(void)
{
    if (!glfwInit()) return 1;

    GLFWwindow* window = glfwCreateWindow(800, 600, "KTE", 0, 0);
    defer { glfwTerminate(); };
    if (!window) return 1;
    defer { glfwDestroyWindow(window); };

    glfwMakeContextCurrent(window);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    defer { ImGui::DestroyContext(); };
    ImGuiIO& io = ImGui::GetIO();
    ImGui::StyleColorsLight();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 130");
    defer { ImGui_ImplOpenGL3_Shutdown(); };
    defer { ImGui_ImplGlfw_Shutdown(); };

    kte::file_browser.SetTitle("Browse files");
    kte::file_browser.SetTypeFilters({".h", ".hpp", ".c", ".cpp", ".cc", ".cxx"});

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        kte::make_toolbar();
        kte::make_editor();

        kte::file_browser.Display();

        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    return 0;
}
