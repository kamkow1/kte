#include <string>
#include <fstream>
#include <sstream>
#include "imgui.h"
#include "imgui_stdlib.h"
#include "editor.h"
#include "imfilebrowser.h"
#include "file_browser.h"
#include "toolbar.h"

std::string kte::editor_buf;
std::string kte::editor_path;

void kte::make_editor(void)
{
    if (kte::file_browser.HasSelected()) {
        kte::editor_path = kte::file_browser.GetSelected().string();
        std::ifstream file;
        file.open(kte::editor_path);
        std::stringstream ss;
        ss << file.rdbuf();
        kte::editor_buf = ss.str();

        kte::file_browser.ClearSelected();
    }

    std::string title;
    title += "Editor [";
    title += kte::editor_path;
    title += "]";

    int flags = ImGuiWindowFlags_NoCollapse
        | ImGuiWindowFlags_NoMove
        | ImGuiWindowFlags_NoResize
        | ImGuiWindowFlags_HorizontalScrollbar;
    ImGui::Begin(title.c_str(), 0, flags);

    ImGuiViewport *vp = ImGui::GetMainViewport();
    ImGui::SetWindowSize(vp->Size);
    ImGui::SetWindowPos(ImVec2(0, kte::toolbar_height));

    std::istringstream iss(kte::editor_buf);

    ImGui::BeginGroup();
    size_t i = 1;
    for (std::string line; std::getline(iss, line);) {
        char buf[16];
        memset(buf, 0, sizeof(buf));
        snprintf(buf, sizeof(buf), "%d", i);
        int height = ImGui::GetFont()->CalcTextSizeA(ImGui::GetFontSize(), FLT_MAX, -1.0f, buf, 0, 0).y;
        ImGui::GetWindowDrawList()->AddText(
            ImVec2(10, kte::toolbar_height+17 + height*i),
            ImColor(0x18, 0x18, 0x18),
            buf
        );
        i += 1;
    }
    ImGui::EndGroup();


    ImGui::SameLine();
    int input_flags = ImGuiInputTextFlags_AllowTabInput; 
    ImGui::InputTextMultiline("Editor", &kte::editor_buf, vp->Size, input_flags);

    ImGui::End();
}

