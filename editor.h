#ifndef KTE_EDITOR_H_
#define KTE_EDITOR_H_

#include <string>

namespace kte {

extern std::string editor_buf;
extern std::string editor_path;

void make_editor(void);

};

#endif // KTE_EDITOR_H_
