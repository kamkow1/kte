#define MIBS_IMPL
#include "mibs.h"

Mibs_Default_Allocator allocator = mibs_make_default_allocator();

#define src \
    "main.cpp", \
    "toolbar.cpp", \
    "file_browser.cpp", \
    "editor.cpp", \
    "imgui/imgui.cpp", \
    "imgui/imgui_draw.cpp", \
    "imgui/imgui_tables.cpp", \
    "imgui/imgui_widgets.cpp", \
    "imgui/backends/imgui_impl_opengl3.cpp", \
    "imgui/backends/imgui_impl_glfw.cpp", \
    "imgui/misc/cpp/imgui_stdlib.cpp"

bool build_kte(void)
{
    bool recom = false;
    static const char* src2[] = { src };
    for (size_t i = 0; i < mibs_array_len(src2); i++) {
        if (mibs_needs_rebuild("./kte", src2[i])) {
            recom = true;
        }
    }

    if (recom) {
        Mibs_Cmd cmd = {0};
        mibs_cmd_append(&allocator, &cmd, "g++");
        mibs_cmd_append(&allocator, &cmd, "-std=c++17");
        mibs_cmd_append(&allocator, &cmd, "-I./imgui/", "-I./imgui/backends", "-I./imgui/misc/cpp/");
        mibs_cmd_append(&allocator, &cmd, "-o", "kte");
        mibs_cmd_append(&allocator, &cmd, src);
        mibs_cmd_append(&allocator, &cmd, "-lm", "-lGL", "-lglfw", "-ldl");
        return mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, 0).ok;
    }
    return true;
}

int main(int argc, char** argv)
{
    mibs_rebuild(&allocator, argc, argv);
    if (!build_kte()) return 1;

    return 0;
}
